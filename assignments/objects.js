const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

// Complete the following underscore functions.
// Reference http://underscorejs.org/ for examples.


function Keys(obj) {
    if (!isObject(obj)) return [];
    var keys = [];
    for (var key in obj) keys.push(key);
    return keys;
}
function isObject(obj) 
{
    var type = typeof obj;
    return type === 'function' || type === 'object' && !!obj;
  }

console.log(Keys(testObject));


function values(obj) {
  // Return all of the values of the object's own properties.
  // Ignore functions
  // http://underscorejs.org/#values

 
    var keys = _keys(obj);
    var length = keys.length;
    var values = Array(length);
    for (var i = 0; i < length; i++) {
      values[i] = obj[keys[i]];
    }
    return values;
  }
function _keys(obj) 
 {
    if (!isObject(obj)) return [];
    if (Object.keys) return Object.keys(obj);
    var keys = [];
    for (var key in obj) if (_.has(obj, key)) keys.push(key);
    return keys;
  }
function isObject(obj)   
{  
    var type = typeof obj;  
    return type === 'function' || type === 'object' && !!obj;  
  } 
  
console.log(values(testObject));

  


function mapObject(obj, cb) {
  // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
  // http://underscorejs.org/#mapObject

  for(const attribute in obj)
  {
      obj [attribute]  = cb(obj[attribute])
  }
  } 
  






function pairs(obj) 
{
  
  
  // Convert an object into a list of [key, value] pairs.
  // http://underscorejs.org/#pairs

 
   var keys = _keys(obj);
   var length = keys.length;
   var pairs = Array(length);
   for (var i = 0; i < length; i++) 
   {
     pairs[i] = [keys[i], obj[keys[i]]];
   }
   return pairs;
 }

function _keys(obj) 
 {
   if (!isObject(obj)) return [];
   if (Object.keys) return Object.keys(obj);
   var keys = [];
   for (var key in obj) if (_.has(obj, key)) keys.push(key);
   return keys;
 }
function isObject(obj) 
{
   var type = typeof obj;
   return type === 'function' || type === 'object' && !!obj;
 }
console.log(pairs(testObject));







function invert(obj) {
  // Returns a copy of the object where the keys have become the values and the values the keys.
  // Assume that all of the object's values will be unique and string serializable.
  // http://underscorejs.org/#invert
 
    var result = {};
    var keys = _keys(obj);
    for (var i = 0, length = keys.length; i < length; i++) {
      result[obj[keys[i]]] = keys[i];
    }
    return result;
  }
function _keys(obj) 
  {
    if (!isObject(obj)) return [];
    if (Object.keys) return Object.keys(obj);
    var keys = [];
    for (var key in obj) if (_.has(obj, key)) keys.push(key);
    return keys;
  }
function isObject(obj) 
 {
    var type = typeof obj;
    return type === 'function' || type === 'object' && !!obj;
  }
console.log(invert(testObject));



function defaults(obj, defaultProps) {
  // Fill in undefined properties that match properties on the `defaultProps` parameter object.
  // Return `obj`.
  // http://underscorejs.org/#defaults
    for(const attribute in defaultProps)
        {
          if(obj [attribute] === undefined)
          {
              obj[attribute] = defaultProps[attribute];
          }

          }
          return obj;
}
